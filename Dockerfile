FROM centos:7

MAINTAINER rizky.faisal12@gmail.com

RUN (curl -sL https://rpm.nodesource.com/setup_12.x | bash -) \
  && yum clean all -y \
  && yum update -y \
  && yum install -y nodejs \
  && yum autoremove -y \
  && yum clean all -y \
  && npm install npm --global

EXPOSE 3000

ENTRYPOINT ["/usr/entrypoint"]

WORKDIR /usr/app
#COPY package.json ./
#COPY package-lock.json ./
#COPY . .
#RUN npm install

COPY entrypoint /usr/entrypoint
